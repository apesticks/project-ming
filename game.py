import pygame, sys, os, pdb, registry
from pygame.locals import *
from item_manager import ItemManager
from room_manager import RoomManager
    
class Game(object):

    def __init__(self):
        pygame.init()
        self.fps = pygame.time.Clock()
        self.running = True

        #sets the screen to appear in same position every time
        self.window_position = 790, 250
        os.environ["SDL_VIDEO_WINDOW_POS"] = str(self.window_position[0]) + "," + str(self.window_position[1])

        
        self.modules = [

            registry.item_manager,
            registry.player,
            registry.instance_manager,
            registry.grid,
            #registry.room_manager,
        ]
        
    def run(self):
        while self.running:
            self.fps.tick(60)
            registry.screen.fill((0,0,0))
            self.events()
            self.update()
            pygame.display.update()

    def events(self):
        for event in pygame.event.get():
            self.game_quit_events(event)
            for module in self.modules:
                if hasattr(module, "events"):
                    module.events(event)

    def game_quit_events(self, event):
        if event.type == QUIT:
            self.running = False
        if event.type == KEYDOWN:
            if event.key == K_ESCAPE:
               self.running = False 
            if event.key == K_h:
                registry.instance_manager.set_active_instance("home")
            if event.key == K_j:
                registry.instance_manager.set_active_instance("level_1")

    def update(self):
        for module in self.modules:
            if hasattr(module, "update"):
                module.update()


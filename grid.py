import pygame, sys, os, pdb, registry
from pygame.locals import *

class Grid(object):
    def __init__(self):
        self.tile_size = 40
        self.rgb = (255,255,255)
        self.on = False
 
    def draw(self):
        for column in range(self.columns):
            x1, y1 = self.tile_size*column + (self.x + self.os_x), self.y + self.os_y 
            x2, y2 = self.tile_size*column + (self.x + self.os_x), self.y + self.height + self.os_y
            pygame.draw.line(registry.screen, (self.rgb), (x1, y1), (x2, y2))

        for row in range(self.rows):
            x1, y1 = self.x + self.os_x, self.tile_size*row + (self.y + self.os_y)
            x2, y2 = self.width + self.x + self.os_x, self.tile_size*row + (self.y + self.os_y)
            pygame.draw.line(registry.screen, (self.rgb), (x1, y1), (x2, y2))         

    def init_instance_variables(self, x, y, os_x, os_y, width, height, columns, rows):
        self.x, self.y = x, y
        self.os_x, self.os_y = os_x, os_y
        self.width, self.height = width, height
        self.columns, self.rows = columns + 1, rows + 1

    def events(self, event):
        if event.type == KEYDOWN: 
            if event.key == K_g:
                self.on = not self.on

    def update(self):
        if self.on == True:
            self.draw()




import pygame, sys, os, pdb, math, registry
from pygame.locals import *
from home import Home
from level_one import LevelOne

class InstanceManager(object):
    def __init__(self):
        self.modules = []
        self.init_instances([Home, LevelOne])
        self.set_active_instance("level_1")

    def init_instances(self, instances):
        for instance in instances:
            self.modules.append(instance())

    def set_active_instance(self, instance_id):
        for instance in self.modules:
            if instance.id == instance_id:
                self.active_instance = instance

    def update_active_instance(self):
        self.active_instance.update()

    def update(self):
        ai = self.active_instance        
        registry.grid.init_instance_variables(registry.player.dx, registry.player.dy, ai.initial_x, ai.initial_y, ai.width, ai.height, ai.columns, ai.rows)
        self.update_active_instance()


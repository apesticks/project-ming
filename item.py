import pygame, sys, os, pdb, random, registry
from pygame.locals import *
from item_properties import SolidProperty, DoorProperty

class Item(object):
    def __init__(self, file_name, x=1, y=1, prop_override={}):
        self.image = pygame.image.load(file_name)
        self.init_property_dict()
        self.set_properties(**prop_override)

        self.coords = x, y
    
        self.width = registry.grid.tile_size
        self.height = registry.grid.tile_size

        self.initial_x = registry.grid.tile_size*x - registry.instance_manager.active_instance.initial_x
        self.initial_y = registry.grid.tile_size*y - registry.instance_manager.active_instance.initial_y
        self.solid_property = SolidProperty(x, y)

    def draw(self):
        self.rect = self.image.get_rect()
        self.x = self.initial_x + registry.player.dx
        self.y = self.initial_y + registry.player.dy
        self.rect.x = self.x
        self.rect.y = self.y
        registry.screen.blit(self.image, (self.rect.x, self.rect.y))

    def init_property_dict(self):
        self.property_dict = {
            "solid": False,
            "movable": False,
            "collectable": False,
        }

    def set_properties(self, **kwargs):
        self.property_dict.update(kwargs)

    def update(self):
        self.draw()
        if self.property_dict["solid"] == True:
            self.solid_property.update()
        
                


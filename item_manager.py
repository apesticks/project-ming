import pygame, sys, os, pdb, registry
from pygame.locals import *
from item import Item

class ItemManager(object):
    def __init__(self):
        pass




    def check_active_instance(self):
        self.ai = registry.instance_manager.active_instance
        self.init_item()

    def init_item(self):
        if self.ai.id == "home":
            if self.ai.items_list == []:
                self.ai.items_list = [
                    Item("items\wall2.png", 5, 5, prop_override={"solid": True}),
                    Item("items\wall2.png", 5, 6, prop_override={"solid": True}),
                    Item("items\wall2.png", 5, 7, prop_override={"solid": True}),
                    Item("items\wall2.png", 6, 7, prop_override={"solid": True}),
                    Item("items\wall2.png", 7, 7, prop_override={"solid": True}),
                    Item("items\wall2.png", 7, 6, prop_override={"solid": True}),
                    Item("items\wall2.png", 9, 7, prop_override={"solid": True}),
                    Item("items\wall2.png", 9, 8, prop_override={"solid": True}),
                    Item("items\wall2.png", 9, 9, prop_override={"solid": True}),
            ]

        if self.ai.id == "level_1":
            if self.ai.items_list == []:
                self.ai.items_list = [
                    Item("items\wall2.png", 5, 5, prop_override={"solid": True}),
                    Item("items\wall2.png", 5, 6, prop_override={"solid": True}),
                    Item("items\wall2.png", 5, 7, prop_override={"solid": True}),
                ]


    def update(self):
        self.check_active_instance()
        for item in self.ai.items_list:
            item.update()
        
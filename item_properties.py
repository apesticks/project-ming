import pygame, sys, os, pdb, random, registry
from pygame.locals import *
from grid import Grid
from tools import pg_caption

class SolidProperty(object):
    def __init__(self, x, y):
        self.image = pygame.image.load("items\wall2.png")
        self.size_x = self.image.get_size()[0]
        self.size_y = self.image.get_size()[1]
        self.pixel_x = registry.grid.tile_size*x
        self.pixel_y = registry.grid.tile_size*y

    def update(self):
        pass
        
class DoorProperty(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.image = pygame.image.load("items\door.png")

    def door_spawn_number(self):
        num = random.randint(1, 4)
        if num == 1:
            self.x = 10
            self.y = 1
        if num == 2:
            self.x = 10
            self.y = 14
        if num == 3:
            self.x = 1
            self.y = 7
        if num == 4:
            self.x = 19
            self.y = 7
        return num

    def check_door_rotation(self):
        num = self.door_spawn_number()
        if num == 3 or num == 4:
            rotation = pygame.transform.rotate(self.image, 90)
            registry.screen.blit(rotation, (100, 100))

    def update(self):
        self.door_spawn_number()
        self.check_door_rotation()
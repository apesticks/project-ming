import pygame, sys, os, pdb, math, registry
from pygame.locals import *

class LevelOne(object):
    def __init__(self):
        self.id = "level_1"
        self.items_list = []
        self.size = registry.screen.get_size()
        self.columns = 20
        self.rows = 20
        self.height = self.columns * registry.grid.tile_size
        self.width = self.rows * registry.grid.tile_size
        self.initial_x = 20
        self.initial_y = 20

    def draw(self):
        self.point_list = self.update_point_list()
        pygame.draw.lines(registry.screen, (255,255,0), False, self.point_list, 1)

    def update_point_list(self):
        return [
            (registry.player.dx + self.initial_x, registry.player.dy + self.initial_y),
            (registry.player.dx + self.initial_x, registry.player.dy + self.height + self.initial_y),
            (registry.player.dx + self.width + self.initial_x, registry.player.dy + self.height + self.initial_y),
            (registry.player.dx + self.width + self.initial_x, registry.player.dy + self.initial_y),
            (registry.player.dx + self.initial_x, registry.player.dy + self.initial_y)
        ]

    def boundary(self):
        if registry.player.rect.left < registry.player.dx + self.initial_x:
            registry.player.dx = registry.player.rect.left - self.initial_x
        if registry.player.rect.right >= registry.player.dx + self.width + self.initial_x:
            registry.player.dx = registry.player.rect.right - self.width - self.initial_x
        if registry.player.rect.top < registry.player.dy + self.initial_y:
            registry.player.dy = registry.player.rect.top - self.initial_y
        if registry.player.rect.bottom >= registry.player.dy + self.height + self.initial_y:
            registry.player.dy = registry.player.rect.bottom - self.height - self.initial_x
    
    def update(self):
        self.draw()
        self.boundary()




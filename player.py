import pygame, sys, os, pdb, math, registry
from pygame.locals import *
from unit_attributes import Rotation
from tools import pg_caption

class Player(object):
    def __init__(self):
        self.angle = 0
        self.image = pygame.image.load("items/player.png")
        self.rect = self.image.get_rect()
        self.rect.x = 388
        self.rect.y = 288
        self.dx = 300
        self.dy = 100

        self.image.set_colorkey((0,0,0))
        self.size = self.image.get_size()[0]
        self.attributes = Rotation()
        
    def draw(self):        
        x, y, center = self.attributes.centered_xy_rotation(self.rect.x, self.rect.y, self.image)
        registry.screen.blit(center, (x, y))

    def move(self):
        key = pygame.key.get_pressed()
        self.move_speed = 1
        if key[K_LSHIFT]:
            self.move_speed = 5
        if key[K_d]:
            self.move_axis(-self.move_speed, 0)
        if key[K_a]:
            self.move_axis(self.move_speed, 0)
        if key[K_s]:
            self.move_axis(0, -self.move_speed)
        if key[K_w]:
            self.move_axis(0, self.move_speed)

    def move_axis(self, dx, dy):
        if dx != 0:
            if not self.check_collision(dx, 0):
                self.dx += dx

        if dy != 0:
            if not self.check_collision(0, dy):
                self.dy += dy

    def check_collision(self, dx, dy):
        x_range = range(self.rect.left, self.rect.right)
        y_range = range(self.rect.top, self.rect.bottom)

        for item in registry.item_manager.ai.items_list:
            if dx != 0:
                next_pixel = self.calc_next_pixel(self.dx, dx, item.initial_x, item, item.width)

                for y in y_range:
                    if y in range(item.initial_y + self.dy, item.initial_y + self.dy + item.height):
                        if next_pixel in x_range:
                            return True
            if dy != 0:
                next_pixel = self.calc_next_pixel(self.dy, dy, item.initial_y, item, item.height)

                for x in x_range:
                    if x in range(item.initial_x + self.dx, item.initial_x + self.dx + item.width):
                        if next_pixel in y_range:
                            return True 
        return False

    def calc_next_pixel(self, distance_moved, increment, initial, item, size):
        next_pixel = distance_moved - 1 + initial
        if increment > 0:
            next_pixel = distance_moved + 1 + initial + size

        return next_pixel


    def update(self):
        self.draw()
        self.move()



        
       



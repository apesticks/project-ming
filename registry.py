import pygame, sys, os, pdb
from pygame.locals import *
from player import Player
from item_manager import ItemManager
#from room_manager import RoomManager
from instance_manager import InstanceManager
from grid import Grid

#sets the screen to appear in same position every time
window_position = 790, 250
os.environ["SDL_VIDEO_WINDOW_POS"] = str(window_position[0]) + "," + str(window_position[1])

screen_size=(800,600)
screen = pygame.display.set_mode(screen_size, 0, 32)

grid = Grid()

player = Player()
instance_manager = InstanceManager()
item_manager = ItemManager()
#room_manager = RoomManager()



    
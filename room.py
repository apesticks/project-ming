import pygame, sys, os, pdb, registry
from pygame.locals import *
from grid import Grid

class Room(object):
    def __init__(self):
        self.size = registry.screen.get_size()
        self.width = 1
        self.limit = 20

        self.grid_trigger = False
        self.active = False

        self.point_list = [
            (self.limit, self.limit), 
            (self.limit, self.size[1] - self.limit),
            (self.size[0] - self.limit, self.size[1] - self.limit),
            (self.size[0] - self.limit, self.limit),
            (self.limit, self.limit)
        ]

        self.side = " "
        self.oposite_side = " "
        self.side_chosen = False


    def draw(self):
        self.rect = pygame.draw.lines(registry.screen, (255,255,0), False, self.point_list, self.width)

    def boundary(self):
        if registry.player.rect.left < self.rect.left: registry.player.rect.left = self.rect.left
        if registry.player.rect.right > self.rect.right: registry.player.rect.right = self.rect.right    
        if registry.player.rect.top < self.rect.top: registry.player.rect.top = self.rect.top
        if registry.player.rect.bottom > self.rect.bottom: registry.player.rect.bottom = self.rect.bottom

    def update(self):
        self.draw()
        self.boundary()




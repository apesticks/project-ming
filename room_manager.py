import pygame, sys, os, pdb, math, registry, random
from pygame.locals import *
from room import Room

class RoomManager(object):
    def __init__(self):

        self.grid_trigger = False
        self.set_room_coords(20, 20)
        self.modules = [Room(), Room()]        
        #self.generate_rooms()

    def set_room_coords(self, x, y):
        self.x = x
        self.y = y

    def generate_rooms(self):
        room_amount = random.randint(1, 2)
        for room in range(room_amount):
            self.modules.append(Room())   

    def check_side_chosen(self):
        for room in self.modules:
            if room.side_chosen == False:
                  room.side = self.asign_side()
                  room.oposite_side = self.asign_oposite_side()
                  room.side_chosen = True

    def asign_side(self):
        sides = ["right", "left", "top", "bottom"]
        random_side = random.choice(sides)
        return random_side

    def asign_oposite_side(self):
        for room in self.modules:
            if room.side == "left":
                return "right"
            if room.side == "right":
                return "left"
            if room.side == "top":
                return "bottom"
            if room.side == "bottom":
                return "top"

    def room_layout(self):
        first_room = None
        second_room = None
        for room in self.modules:
            if first_room == None:
                first_room = room
            if second_room == None:
                second_room = room
            if first_room != None and second_room != None:
                second_room.rect.right = first_room.rect.left
            first_room = second_room
            second_room = None

    def events(self, event):
        if event.type == KEYDOWN:
            if event.key == K_g:
                self.grid_trigger = not self.grid_trigger

    def update(self):
        for room in self.modules:
            room.update()
        if self.grid_trigger == True:
            registry.grid.draw()
        self.check_side_chosen()
        self.room_layout()

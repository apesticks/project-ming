import pygame, sys, os, pdb, math
from pygame.locals import *

class Rotation(object):
    def __init__(self):
        pass
    
    def centered_xy_rotation(self, x, y, image):
        """returns the centered xy calculation"""
        width1, width2, height1, height2 = self.calc_rotation(x, y, image)
        centered_x = x - (width2 - width1)/2
        centered_y = y - (height2 - height1)/2
        return centered_x, centered_y, self.centered_image
  
    def calc_rotation(self, x, y, image):
        """calculates the difference between two images once rotated"""
        self.angle = self.calc_angle(x, y)
        width1, height1 = image.get_size()
        self.centered_image = pygame.transform.rotate(image, self.angle)
        width2, height2 = self.centered_image.get_size()
        return width1, width2, height1, height2
   
    def calc_angle(self, x, y):
        """calculates the angle to rotate"""
        x, y, r = self.calc_triangle(x, y)
        radians = math.atan2(y, x)
        angle = math.degrees(radians)
        return angle    
   
    def calc_triangle(self, x, y):
        """finds the xyr of a triangle based on mouse position and set location"""
        mouse_pos = pygame.mouse.get_pos()
        x = -(x - mouse_pos[0])
        y = (y - mouse_pos[1])
        r = math.sqrt(y**2 + x**2)
        return x, y, r 

